An SWI Prolog library to create a graph from an RDF/Turtle file. 

It uses the Graphviz's dot utility to create the image file.

[Add-on page on SWI Prolog site](https://www.swi-prolog.org/pack/list?p=ontodot)

# Install
Run swipl and type:

```prolog
pack_install(ontodot).
```

# Usage

![](http://crowd.fi.uncoma.edu.ar/wiki/lib/exe/fetch.php/christian:software:ontodot.pl.gif)

[See video in Peertube](https://peertube.cipherbliss.com/videos/watch/b10e520e-f051-4bf4-a1f5-0ba0f48e17de)

# License
![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This work is under the GPL v3 License.

See COPYING.txt file or [the GNU GPL License page](https://www.gnu.org/licenses/gpl.html).
